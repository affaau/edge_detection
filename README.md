To create repository from existing project in local system

1. Firstly, create an empty repository in bitbucket. Prefer to be empty, not even with README

2. Switch to your project directory
   $> cd /path/to/your/repo

3. Prepare for pushing to git repository
   $> git init
   $> git add --all
   $> git commit -m 'commit message'

4. Connect your existing repository to Bitbucket
   $> git remote add origin https://username@bitbucket.org/username/new_repo.git 
   $> git push -u origin master

Ref: http://kbroman.org/github_tutorial/pages/init.html
