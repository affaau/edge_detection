#! usr/bin/python3

##
## Ref:
## https://shahsparx.me/edge-detection-opencv-python-video-image/ 
##
## Edge detection demo using Canny edge detector operator
##

import cv2
import numpy as np

cv2.namedWindow('Original', cv2.WINDOW_AUTOSIZE)
cv2.namedWindow('Laplacian Filter', cv2.WINDOW_AUTOSIZE)

img = cv2.imread('mask.jpg',cv2.IMREAD_GRAYSCALE)
##img = cv2.imread('kitchen.jpg',cv2.IMREAD_GRAYSCALE)
 
filter = cv2.Canny(img,100,200)
 
cv2.imshow('Original', img)
cv2.imshow('Laplacian Filter', filter)
 
cv2.waitKey(0)
cv2.destroyAllWindows()