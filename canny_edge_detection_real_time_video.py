#! /usr/bin/python3

##
## Ref:
## https://shahsparx.me/edge-detection-opencv-python-video-image/ 
##
## Edge detection demo using Canny edge detector operator
##

import numpy as np
import cv2
 
cv2.namedWindow('Original', cv2.WINDOW_NORMAL)
cv2.namedWindow('Edges', cv2.WINDOW_NORMAL)
 
cap = cv2.VideoCapture(0)
 
while(1):
  ret, frame = cap.read()
  gray_vid = cv2.cvtColor(frame, cv2.IMREAD_GRAYSCALE)
  cv2.imshow('Original',frame)
  edged_frame = cv2.Canny(frame,100,200)
  cv2.imshow('Edges',edged_frame)
  k= cv2.waitKey(5) & 0xFF
  ## ESC key
  if k==27:
    break
cap.release()
cv2.destroyAllWindows()